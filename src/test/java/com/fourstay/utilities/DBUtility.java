package com.fourstay.utilities;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import org.junit.Test;

public class DBUtility {
// JDBC
// oracle -> oracle jdbc driver
// mySQL -> mysql driver
	
	@Test
	public void getDBData() throws ClassNotFoundException, SQLException{
		// jdbc
		Class.forName("com.mysql.jdbc.Driver");
		// establish connection to DB
		// in order to connect you need to provide the ip adress+db_name, username, pass
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_tracker","root","12345");
				
		assertNotNull(conn);	
		//execute a query
		Statement st = conn.createStatement();
		//statement is what you send to DB to execute
		
		
		//parse the query result
		ResultSet res = st.executeQuery("select * from employees");
		//resultSet is its own type, it is a little hard to use and read
		
		res.getMetaData(); //-> metaData is data about data(description of some sort of data)
		//result is stored in the metadata
		
		while(res.next()){
			//next iterates through rows
			
			int cols = res.getMetaData().getColumnCount();
			// getColumnCount() does not return total number of cols in whole table, but only in the result
			
			System.out.println(res.getMetaData().getColumnCount());
			for (int i = 1; i <= cols; i++) {
				System.out.println(res.getMetaData().getColumnName(i));
	//			System.out.println(res.getMetaData().getColumnType(i));
			}
			
	//		res.getst
		while(res.next()){
	//		System.out.println(res.getString(columnIndex));
		}
		}
	}
}
