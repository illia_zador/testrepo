package com.fourstay.utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.fourstay.pages.HomePage;
import com.fourstay.pages.SearchResultsPage;

import cucumber.api.Scenario;
import cucumber.api.java.After;
//import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hook {
	private static WebDriver driver;
	 
	
	@Before
	public void setDriver(){
	if(driver== null || ((RemoteWebDriver)driver).getSessionId() == null){	
		System.setProperty(TestInputs.CHROME_DRIVER, TestInputs.CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
		}
	Base.driver = driver;
	}

	
	@After
	public void tearDown(Scenario scenario){
	//take a screenshot if scenario fails	
		
		if(scenario.isFailed()){
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png");
		}
		if(driver!=null){
			driver.quit();
			}
	}
	
}
