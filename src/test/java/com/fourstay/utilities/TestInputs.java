package com.fourstay.utilities;

public class TestInputs {
	// I don't want to instantiate an object to read these variables, and they need to be visible to 
	// all classes in the project
	
	public static final String BASE_URL = "http://ec2-52-37-67-115.us-west-2.compute.amazonaws.com:3000/";
	public static final String GUEST_USER_ID = "testguestuserb@zain.site";
	public static final String GUEST_USER_PWD = "password";
	public static final String GUEST_FIRST_NAME = "TestGuest";
//	public static final String GUEST_LAST_NAME = "UserB";

	
	public static final String CHROME_DRIVER = "webdriver.chrome.driver";
	public static final String CHROME_DRIVER_PATH = "/Users/illiazador/Desktop/Libraries/chromedriver";
	
	public static final String HOST_USER_ID = "welarof@zain.site";
	public static final String HOST_USER_PWD = "password";
//	public static final String HOST_FIRST_NAME = "TestHost";
//	public static final String HOST_LAST_NAME = "UserA";
	public static final String HOST_FIRST_NAME = "ZZZ";
	public static final String HOST_LAST_NAME = "z";
	
		}
