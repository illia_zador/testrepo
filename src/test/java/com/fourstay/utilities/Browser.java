package com.fourstay.utilities;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Browser extends Base{

	public static void verifyText( String myText){
		if(myText.isEmpty()){
			fail("Empty text was passed to VerifyText method");
		}

		try{
			assertTrue("Text is not displayed: "+myText,
					driver.findElement(By.xpath("//*[.='"+myText+"']")).isDisplayed());
		}catch(Exception e){
			fail("Text is not displayed: "+ myText+ "->Element not found");
		}
	}


	public static void verifyPartialText( String myText){
		if(myText.isEmpty()){
			fail("Empty text was passed to VerifyText method");
		}
		try{
			String xpath1 = "//*[contains (text(),'"+myText+"')]";
			waitFor(By.xpath(xpath1));
			
	
			assertTrue("Partial text is not displayed: "+myText,
					driver.findElement(By.xpath("//*[contains (text(),'"+myText+"')]")).isDisplayed());
		}catch(Exception e){
			fail("Partial text is not displayed: "+ myText + "->Element not found");
		}

	}
	public static void waitFor(By by){
		WebDriverWait wait = new WebDriverWait(driver, 10);
		ExpectedCondition <WebElement> con = ExpectedConditions.visibilityOfElementLocated(by);
		wait.until(con);
	}

	}

