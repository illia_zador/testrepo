package com.fourstay.step_definitions;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.fourstay.pages.HomePage;
import com.fourstay.pages.SearchResultsPage;
import com.fourstay.utilities.Base;
import com.fourstay.utilities.Browser;
import com.fourstay.utilities.TestInputs;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefsTwo extends Base{
	HomePage hp = new HomePage();
	SearchResultsPage sr = new SearchResultsPage(); 
	
	@Given("^I am on FourStay homepage$")
	public void i_am_on_FourStay_homepage() throws Throwable {
		driver.get(TestInputs.BASE_URL);

	}

	@Then("^I click on login link$")
	public void i_click_on_login_link() throws Throwable {
		
	try{	
		assertTrue("Home page was not loaded",hp.fourStayLogo.isDisplayed());
		
	}catch(Exception e ){
		fail("Home page was not loaded");
	}
	hp.loginLink.click();
	
	}

	@Then("^I enter \"([^\"]*)\" credentials$")
	public void i_enter_credentials(String role) throws Throwable {
			
		
		hp.emailInputField.sendKeys(TestInputs.HOST_USER_ID);
		hp.passInputField.sendKeys(TestInputs.HOST_USER_PWD);
		hp.loginButton.click();

	}

	@Then("^I should see Other Listing by message$")
	public void i_should_see_Other_Listing_by_message() throws Throwable {
		Browser.verifyText( "Other Listing by "+TestInputs.HOST_FIRST_NAME+" "+TestInputs.HOST_LAST_NAME);
	}	
	
	@Then("^I should see \"([^\"]*)\" message$")
	public void i_should_see_message(String text) throws Throwable {
	    Browser.verifyPartialText( text);
	    
	}
	@When("^I login with email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_login_with_email_and_password(String email, String pass) throws Throwable {
	    hp.login(email, pass);
	   
	}
}
