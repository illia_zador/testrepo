package com.fourstay.step_definitions;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.fourstay.utilities.Base;
import com.fourstay.utilities.Browser;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DemoStepDefs extends Base{

//	WebDriver driver;

	@After
	public void tearDown() {
		if(driver!=null){
		driver.quit();
		}
	}

	@Given("^I am on the Google page$")
	public void i_am_on_the_Google_page() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "/Users/illiazador/Desktop/Libraries/chromedriver");
	
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://google.com");
	}

	@When("^I enter a search term$")
	public void i_enter_a_search_term() throws Throwable {
		driver.findElement(By.name("q")).sendKeys("Selenium" + Keys.ENTER);

	}

	@Then("^I should see my search results$")
	public void i_should_see_my_search_results() throws Throwable {
		Thread.sleep(2000);
		assertTrue(driver.getTitle().endsWith("Google Search"));
		assertTrue(driver.findElement(By.id("resultStats")).isDisplayed());

	}

	@When("^I search for multiple keywords$")
	public void i_search_for_multiple_keywords() throws Throwable {
		driver.findElement(By.name("q")).sendKeys("Selenium cucumber jobs" + Keys.ENTER);

	}

	@Then("^I should see search results count$")
	public void i_should_see_search_results_count() throws Throwable {
		Thread.sleep(2000);
		assertTrue(driver.getTitle().endsWith("Google Search"));
		assertTrue(driver.findElement(By.id("resultStats")).isDisplayed());

	}

	@And("^I should see keyword in title$")
	public void i_should_see_keyword_in_title() {
		assertTrue(driver.getTitle().toLowerCase().contains("selenium cucumber jobs"));
	}
}
