package com.fourstay.step_definitions;

import static org.junit.Assert.assertTrue;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestDemoStepDefs {
	int start, ate, leftResult;

	@Given("^there are (\\d+) cucumbers$")
	public void there_are_cucumbers(int start) throws Throwable {
		this.start = start;

	}

	@When("^I eat (\\d+) cucumbers$")
	public void i_eat_cucumbers(int ate) throws Throwable {
		this.ate = ate;

	}

	@Then("^I should have (\\d+) cucumbers$")
	public void i_should_have_cucumbers(int left) throws Throwable {
		this.leftResult = start - ate;
		assertTrue(left == leftResult);

	}
}
