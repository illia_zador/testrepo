package com.fourstay.step_definitions;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.fourstay.pages.HomePage;
import com.fourstay.utilities.Base;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDefs extends Base{

	HomePage hp = new HomePage();

	@When("^I click on the beds dropdown list$")
	public void i_click_on_the_beds_dropdown_list() throws Throwable {
	hp.beds.click();
	}

	@Then("^I should see the following bed <options>$")
	public void i_should_see_the_following_bed_options(List<String> inputOptions) throws Throwable {
		String css = ".list-option-select";
		List<WebElement> eList = driver.findElements(By.cssSelector(css));

		ArrayList<String> actualOptions = new ArrayList<String>();
		for(WebElement elem : eList){
			actualOptions.add(elem.getText());
		}

		for(int i=0; i<inputOptions.size(); i ++){
			assertEquals(inputOptions.get(i), actualOptions.get(i));
		}

	}
//	@Then("^I should see the following bed <options>$")
//	public void i_should_see_the_following_bed_options2(List<String> str) throws Throwable {
//		
//		for (String string : str) {
//			System.out.println(string);
//		}
//	}

}
















