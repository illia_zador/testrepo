package com.fourstay.step_definitions;

import static org.junit.Assert.*;

import com.fourstay.pages.HomePage;
import com.fourstay.utilities.Base;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FourStayNegativeLoginStep extends Base{
	
	HomePage hp = new HomePage();
	
	@When("^I enter email \"([^\"]*)\" and password \"([^\"]*)\"$")
	public void i_enter_email_and_password(String email, String password) throws Throwable {
	   hp.enterCredentials(email, password);
	   
	}

	@Then("^login button should be disabled$")
	public void login_button_should_be_disabled() throws Throwable {
	    assertFalse("Login Button is enabled",hp.loginButtonEnabled());
	   
	}

}
