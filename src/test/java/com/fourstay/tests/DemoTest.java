package com.fourstay.tests;

import static org.junit.Assert.*;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DemoTest {
	
	
	@Test
	public void testTitle(){
		System.setProperty("webdriver.chrome.driver", "/Users/illiazador/Desktop/Libraries/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		assertEquals("Google", driver.getTitle());
	}
}
