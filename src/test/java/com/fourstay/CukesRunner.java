package com.fourstay;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(	plugin = {"pretty", "html:target/html/","json:target/cuckes2.json"},
					tags = {"@git","~@TestGoogle"},
					dryRun = false,
				
				 	glue = "/Users/illiazador/Documents/workspace/4stay-uat/src/test/java/com/fourstay/step_definitions")


public class CukesRunner {

	
}