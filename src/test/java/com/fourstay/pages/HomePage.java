package com.fourstay.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.fourstay.utilities.Base;

public class HomePage extends Base{
	
	public HomePage (){
		// so the driver can see all the elements in this class, comes with selenium
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//img[@alt='HStay Logo']")
	public WebElement fourStayLogo;
	
	@FindBy(xpath="//li[@data-target='#modal-login']")
	public WebElement loginLink;
	
	@FindBy(id="btn-login")
	public WebElement loginButton;
	
	@FindBy(xpath="//*[.='Something went wrong']")
	public WebElement wentWrongError;
	
	@FindBy(id="email")
	public WebElement emailInputField;
	
	@FindBy(name="Password")
	public WebElement passInputField;
	
	@FindBy(name="iLocName")
	public WebElement schoolNameInput;
	
	@FindBy(id="rentoutfrom")
	public WebElement startRentDate;
	
	@FindBy(id="rentoutto")
	public WebElement endRentDate;
	
	@FindBy(name="no_of_beds")
	public WebElement numOfBeds;
	
	@FindBy(id="themates")
	public WebElement beds;
	
	@FindBy(id="search")
	public WebElement searchButton;
	
	public void enterCredentials(String email, String pass){
		emailInputField.sendKeys(email);
		passInputField.sendKeys(pass);	
	}
	public boolean loginButtonEnabled(){
		 return loginButton.isEnabled();
	}
	public void login(String email, String pass) throws Throwable {
		emailInputField.sendKeys(email);
		passInputField.sendKeys(pass);
		loginButton.click();
	}
}
