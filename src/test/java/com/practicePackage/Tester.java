package com.practicePackage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Tester extends Base{

	public static void main(String[] args) {
		System.setProperty(prop.getProperty("driver"), prop.getProperty("driverPath"));
		WebDriver driver = new ChromeDriver();
		System.out.println(prop.getProperty("baseUrl"));
		driver.get(prop.getProperty("baseUrl"));

	}

}
