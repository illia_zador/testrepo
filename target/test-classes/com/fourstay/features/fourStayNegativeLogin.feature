Feature: FourStay login with invalid credentials 

As a Guest, I should see login button disabled when I enter incorrect email format
As a Guest, I should see login button disabled when I enter email is blank
As a Guest, I should see login button disabled when I enter password is blank
As a Guest, I should be able to see an error message when I try to login with a wrong email
As a Guest, I should be able to see an error message when I try to login with a wrong password
As a Host, I should be able to see an error message when I try to login with a wrong email
As a Host, I should be able to see an error message when I try to login with a wrong password

@A123
Scenario: Guest-> login button disabled with incorrect email format 

	Given I am on FourStay homepage 
	Then I click on login link 
	When I enter email "testguestuserbzain.site" and password "password" 
	Then login button should be disabled 
	
@A123
Scenario: Guest-> login button disabled with blank email 

	Given I am on FourStay homepage 
	Then I click on login link 
	When I enter email "" and password "password" 
	Then login button should be disabled 
	
@A123
Scenario: Guest-> login button disabled with blank password 

	Given I am on FourStay homepage 
	Then I click on login link 
	When I enter email "testguestuserb@zain.site" and password "" 
	Then login button should be disabled 
	
@A123
Scenario: Error message with wrong email 
	Given I am on FourStay homepage 
	Then I click on login link 
	When I login with email "testguestuserb@yahoo.com" and password "password" 
	Then I should see "Something went wrong" message 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
